variable "storage_container_name" {
  type        = string
  description = "Name for the container"
}

variable "resource_group" {
  type        = string
  description = "Resource Group previously deployed, name"
}

variable "storage_account" {
  type        = string
  description = "Storage account previously deployed, name"
}

variable "container_access_type" {
  type        = string
  description = "Access type for the storage container"
  default     = "private"
}

variable "storage_blob_name" {
  type        = string
  description = "Name for the Blob"
}

variable "storage_blob_size" {
  type        = string
  description = "Size for the Blob"
  default     = "5120"
}

variable "storage_blob_type" {
  type        = string
  description = "Type for the Blob"
  default     = "Page"
}

# aAzure Blob Storage Module

- Creates: 
- Storage container
- Storage Blob

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| stroage_blob_type | The type of the storage blob to be created. Possible values are Append, Block or Page | Page | - |:no:|
| stroage_blob_size | Size of the blob | 5120 | - |:no:|
| container_access_type | Access type for the storage container | Private | - |:no:|

# Outputs 

| Name | Description |
|------|-------------|
| storage_contianer | ID of the group created |
| storage_blob | ID of the group created |
| storage_blob_url | ID of the group created |

# Usage

  

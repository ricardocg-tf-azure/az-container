terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.20.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "50505e05-cd97-4b8d-a080-40b5b880ce44"
  tenant_id       = "34f55fb5-3a46-4626-b706-5d94d061b0d4"
}

#Generate random name for security group
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = module.resource_group.name
    }

    byte_length = 8
}

module "resource_group" {
  source   = "git::https://gitlab.com/ricardocg-tf-azure/az-resource-group.git?ref=master"
  name     = "myResourceGroup"
  location = "eastus"
  env      = "test"
}

module "storage_account" {
  source   = "git::https://gitlab.com/ricardocg-tf-azure/az-storage-account.git?ref=master"
  name     = "diag${random_id.randomId.hex}"
  rg       = module.resource_group.name
  location = "eastus"
  env      = "test"
}

module "blob_storage_example" {
  source                 = "../../../"
  storage_container_name = "blobtest1212"
  storage_blob_name      = "testfafaiblob1212"
  resource_group         = module.resource_group.name
  storage_account        = module.storage_account.name
}


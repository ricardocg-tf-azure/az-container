output "storage_contianer" {
  value       = azurerm_storage_container.blob_storage.name
  description = "The generated name for the storage container"
}

output "storage_blob" {
  value       = azurerm_storage_blob.blob.name
  description = "The generated name for the blob storage"
}

output "storage_blob_url" {
  value       = azurerm_storage_blob.blob.url
  description = "The generated URL for the blob storage"
}

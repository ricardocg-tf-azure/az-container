terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.20.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "50505e05-cd97-4b8d-a080-40b5b880ce44"
  tenant_id       = "34f55fb5-3a46-4626-b706-5d94d061b0d4"
}

resource "azurerm_storage_container" "blob_storage" {
  name                  = var.storage_container_name
  #resource_group_name   = var.resource_group
  storage_account_name  = var.storage_account
  container_access_type = var.container_access_type
}

resource "azurerm_storage_blob" "blob" {
  name                   = var.storage_blob_name
  storage_account_name   = var.storage_account
  storage_container_name = azurerm_storage_container.blob_storage.name
  type                   = var.storage_blob_type
  size                   = var.storage_blob_size
}